# main.py
import sys
import cv2 as cv
import numpy as np
import math
import queue


def nothing(x):
    pass


def translation(img):
    tx = sys.argv[3]
    ty = sys.argv[4]
    transMat = np.float32([[1, 0, tx], [0, 1, ty]])
    cols = img.shape[1]
    rows = img.shape[0]
    return cv.warpAffine(img, transMat, (cols, rows))


def rotationTrackbar(cols, row, angle, scale):
    cv.createTrackbar('x', 'img', 0, cols-1, nothing)
    cv.createTrackbar('y', 'img', 0, row-1, nothing)
    cv.createTrackbar('angle', 'img', angle, 360, nothing)
    cv.createTrackbar('scale', 'img', scale, 100, nothing)


def rotation(img):
    centerx = float(sys.argv[3])
    centery = float(sys.argv[4])
    angle = int(sys.argv[5])
    scale = int(sys.argv[6])
    cols = img.shape[1]
    rows = img.shape[0]
    dst = img
    cv.imshow('img', dst)
    rotationTrackbar(cols, rows, angle, scale*10)
    while(1):
        cv.imshow('img', dst)
        """angle += 1
        centerx = (centerx +1) % cols
        centery = (centery +1) % rows"""
        angle = cv.getTrackbarPos('angle', 'img')
        scale = cv.getTrackbarPos('scale', 'img')/10
        centerx = cv.getTrackbarPos('x', 'img')
        centery = cv.getTrackbarPos('y', 'img')
        rotMat = cv.getRotationMatrix2D((centerx, centery), angle, scale)
        dst = cv.warpAffine(img, rotMat, (cols, rows))
        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()


def rotationTrackbar2(angle):
    cv.createTrackbar('angle', 'img', angle, 360, nothing)


def rotation2(img):
    centerx = float(sys.argv[3])
    centery = float(sys.argv[4])
    angle = math.radians(float(sys.argv[5]))
    scale = int(sys.argv[6])
    # scale = int(sys.argv[6])

    dst = np.zeros_like(img)
    cv.imshow('img', dst)

    R = np.float32([[math.cos(angle), -math.sin(angle)],
                    [math.sin(angle), math.cos(angle)]])
    cv.imshow('img', dst)
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):

            [xp, yp] = np.matmul(R, np.float()
            xp=(0 <= xp < img.shape[0])*xp
            yp=(0 <= yp < img.shape[1])*yp


            dst[int(xp), int(yp)]=img[x, y]
            # angle += 1
    while(1):
        k=cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()


def main(argv):
    filename=sys.argv[1]
    img=cv.imread(filename)
    tr=sys.argv[2]
    if tr == 't':
        dst=translation(img)
    elif tr == 'r':
        rotation(img)
    elif tr == 'r2':
        rotation2(img)


if __name__ == "__main__":
    main(sys.argv[1:])
