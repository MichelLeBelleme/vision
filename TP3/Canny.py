# main.py
import sys
import cv2 as cv
import numpy as np
import math
import  queue

def nothing(x):
	pass

def gaussianFiltering(img_in,s):
    img_out = img_in.copy()
    n = math.floor(3*s)
    if n%2 == 0 :
        n+=1
    elif n<= 0:
        n=3
    #print(n)
    img_out = cv.GaussianBlur(img_in, (n,n), s)
    return img_out

def computeGx(img_in):
    matrice = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
    sobelx = cv.filter2D(img_in, cv.CV_64F, matrice)
    return sobelx

def computeGy(img_in):
    matrice = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
    sobely = cv.filter2D(img_in,cv.CV_64F, matrice)
    return sobely

def computeMagnitude(Gx,Gy):
    Gx = Gx.astype(np.uint32)
    Gy = Gy.astype(np.uint32)
    x2 = np.power(Gx,2)
    y2 = np.power(Gy,2)
    x2y2 = np.add(x2,y2)
    magnitude = np.sqrt(x2y2)
    magnitude = np.add((magnitude<=255)*magnitude,(magnitude>255)*255).astype(np.uint8)
    return magnitude

def computeDirection(Gx,Gy):
    direction =  np.arctan2(Gy,Gx)
    #print(direction)
    return direction 

def removeNonMaxima(grad_m, grad_d):
    maskinf = (grad_d<(-math.pi /8))
    masksup = (grad_d>=((7*math.pi) /8))
    nonmaxima = grad_m.copy()
    angle= grad_d.copy()
    angle[maskinf] = (grad_d+math.pi)[maskinf]
    angle[masksup] = (grad_d-math.pi)[masksup]
    for i in  range(grad_m.shape[0]):
      for j in  range(grad_m.shape[1]):
            if((3*math.pi /8) <= angle[i,j] < (5*math.pi /8)):
                if((i-1>0 and  (grad_m[i,j]<grad_m[i-1,j]))  or (i+1<grad_m.shape[0] and  (grad_m[i,j]<grad_m[i+1,j]))): 
                    nonmaxima[i,j] = 0
            if((math.pi /8) <= angle[i,j] < (3*math.pi /8)):
                if((i+1<grad_m.shape[0] and  j+1<grad_m.shape[1] and  (grad_m[i,j]<grad_m[i+1,j+1]))  or (i-1>0 and  j-1>0 and  (grad_m[i,j]<grad_m[i-1,j-1])) ): 
                    nonmaxima[i,j] = 0
            if((-math.pi /8) <= angle[i,j] < (math.pi /8)):
            
                if((j-1>0 and  (grad_m[i,j]<grad_m[i,j-1]))  or (j+1<grad_m.shape[1] and   (grad_m[i,j]<grad_m[i,j+1])) ): 
                    nonmaxima[i,j] = 0
            if((5*math.pi /8 )<= angle[i,j] < (7*math.pi /8)):
                if((i+1<grad_m.shape[0] and  j-1>0 and  (grad_m[i,j]<grad_m[i+1,j-1])) or (i-1>0 and  j+1<grad_m.shape[1] and  (grad_m[i,j]<grad_m[i-1,j+1])) ): 
                    nonmaxima[i,j] = 0

    return nonmaxima
                

def computeThresholds(grad_maxima,  alpha,  beta):
    t= np.sort(np.ravel(grad_maxima))
    n = t.size
    """print(alpha*n)
    print(n)
    print(grad_maxima.shape[0]*grad_maxima.shape[1])
    print(t)
    print(round(alpha*n))
    print(t[round(alpha*n)])"""
    thigh = t[round(alpha*n)]
    tlow = beta * thigh
    return (tlow,thigh)

def hysteresisThresholding(grad_maxima, tLow,tHigh):
    canny = np.zeros(grad_maxima.shape)
    q = queue.Queue()
    for i in  range(grad_maxima.shape[0]):
      for j in  range(grad_maxima.shape[1]):
        if(grad_maxima[i,j]>=tHigh):
            q.put((i,j))
            canny[i,j] = 255
        else :
            canny[i,j] = 0

    while (not q.empty()):
        (pi,pj) = q.get()
        if(pi-1>0) : 
            if((grad_maxima[pi-1,pj])>=tLow and canny[pi-1,pj]==0):
                 canny[pi-1,pj] = 255
                 q.put((pi-1,pj))
            if(pj-1>0 and (grad_maxima[pi-1,pj-1])>=tLow and canny[pi-1,pj-1]==0):
                 canny[pi-1,pj-1] = 255
                 q.put((pi-1,pj-1))       
            if(pj+1<grad_maxima.shape[1] and (grad_maxima[pi-1,pj+1])>=tLow and canny[pi-1,pj+1]==0):
                 canny[pi-1,pj+1] = 255
                 q.put((pi-1,pj+1))    
        if(pi+1<grad_maxima.shape[0]) : 
            if((grad_maxima[pi+1,pj])>=tLow and canny[pi+1,pj]==0):
                 canny[pi+1,pj] = 255
                 q.put((pi+1,pj))
            if(pj-1>0 and (grad_maxima[pi+1,pj-1])>=tLow and canny[pi+1,pj-1]==0):
                 canny[pi+1,pj-1] = 255
                 q.put((pi+1,pj-1))       
            if(pj+1<grad_maxima.shape[1] and (grad_maxima[pi+1,pj+1])>=tLow and canny[pi+1,pj+1]==0):
                 canny[pi+1,pj+1] = 255
                 q.put((pi+1,pj+1))
        if(pj-1>0 and (grad_maxima[pi,pj-1])>=tLow and canny[pi,pj-1]==0):
            canny[pi,pj-1] = 255
            q.put((pi,pj-1))       
        if(pj+1<grad_maxima.shape[1] and (grad_maxima[pi,pj+1])>=tLow and canny[pi,pj+1]==0):
            canny[pi,pj+1] = 255
            q.put((pi,pj+1))
    return canny



      
    
def display():
    cv.createTrackbar('sigma','blur',20,150,nothing)
    cv.createTrackbar('alpha','blur',95,99,nothing)
    cv.createTrackbar('beta','blur',80,99,nothing)

def main(argv):
    filename = sys.argv[1]
    img = cv.imread(filename)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY )
    blur = img
    cv.imshow("blur", blur)
    gx = img
    #cv.imshow("gx", gx)
    gy = img
   # cv.imshow("gy", gy)
    magnitude = img
    cv.imshow("magnitude", magnitude)
    direction = img
    #cv.imshow("direction", direction)
    nonMaxima = img
    #cv.imshow("nonMaxima", nonMaxima)
    canny = img
    cv.imshow("canny", canny)
    realcanny = img
    cv.imshow("realcanny", realcanny)
    display()
    while(1):
        cv.imshow("blur", blur)
        #cv.imshow("gx", gx)
        #cv.imshow("gy", gy)
        cv.imshow("magnitude", magnitude)
        #cv.imshow("direction", direction)
        #cv.imshow("nonMaxima", nonMaxima)
        cv.imshow("canny", canny)
        cv.imshow("realcanny", realcanny)
        cv.imwrite("blur.png", blur)
        cv.imwrite("magnitude.png", magnitude)
        cv.imwrite("canny.png", canny)
        sigmaX = cv.getTrackbarPos('sigma','blur')/10
        alpha = cv.getTrackbarPos('alpha','blur')/100
        beta = cv.getTrackbarPos('beta','blur')/100
        blur = gaussianFiltering(gray,sigmaX)
        gx = computeGx(blur)
        gy = computeGy(blur)
        magnitude = computeMagnitude(gx,gy)
        direction = computeDirection(gx,gy)
        nonMaxima = removeNonMaxima(magnitude,direction)
        (tLow,tHigh) = computeThresholds(nonMaxima,  alpha,  beta)
        print("Seuil haut :",tHigh,"Seuil bas",tLow)
        canny = hysteresisThresholding(nonMaxima, tLow,tHigh)
        realcanny = cv.Canny(blur, tLow,tHigh,apertureSize =3,L2gradient=True)
        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()






if __name__ == "__main__":
    main(sys.argv[1:])