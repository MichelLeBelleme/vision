# main.py
import sys
import cv2 as cv
import numpy as np



def main(argv):

    filename = sys.argv[1]
    img = cv.imread(filename)
    gray = cv.cvtColor(img,cv.COLOR_RGB2GRAY)
    m = np.array([[0,0,0],[0,1,0],[0,0,0]])
    matrice = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
    matrice2 = np.array([[-1,-1,-1],[-1,9,-1],[-1,-1,-1]])
    imgfilt1= cv.filter2D(gray, cv.CV_16S, matrice)
    ret,thresh1 = cv.threshold(imgfilt1,255,255,cv.THRESH_TRUNC)
    ret,thresh12 = cv.threshold(thresh1,0,255,cv.THRESH_TOZERO)
    img2 = (thresh12).astype(np.uint8) 
    
    imgfilt2= cv.filter2D(gray, cv.CV_16S, matrice2)
    ret,thresh2 = cv.threshold(imgfilt2,255,255,cv.THRESH_TRUNC)
    ret,thresh21 = cv.threshold(thresh2,0,255,cv.THRESH_TOZERO)
    img3 = (thresh21).astype(np.uint8) 
    
    cv.imshow("image 1", gray)
    cv.imshow("image 2", img2)
    cv.imshow("image 3", img3)
    cv.waitKey(0)
    








if __name__ == "__main__":
    main(sys.argv[1:])

