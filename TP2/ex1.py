# main.py
import sys
import cv2 as cv
import numpy as np


def main(argv):
    n = int(sys.argv[2])
    filename = sys.argv[1]
    img = cv.imread(filename)
    print(n)
    matrice = np.ones((n, n))*(1/9)
    print(matrice)
    img2 = cv.filter2D(img, -1, matrice)
    cv.imshow("image 1", img)
    cv.imshow("image 2", img2)
    cv.waitKey(0)
    








if __name__ == "__main__":
    main(sys.argv[1:2])
