# main.py
import sys
import cv2 as cv
import numpy as np
import math

def show(x):
	pass


def display():
    cv.namedWindow('controls')
    cv.createTrackbar('Sigma','controls',1,150,show)

def main(argv):
    display()
    filename = sys.argv[1]
    img = cv.imread(filename)
    while(1):
     
        sigmaX = int(cv.getTrackbarPos('Sigma','controls'))/366.12
        n = math.floor(3*sigmaX)
        if n%2 == 0 :
            n+=1
        elif n<= 0:
            n=3

        dst = cv.GaussianBlur(img, (n,n), sigmaX)
        cv.imshow("image 1", img)
        cv.imshow("image 2", dst)
        k = cv.waitKey(0)
        print(k)
        if k == 113:
            break
    








if __name__ == "__main__":
    main(sys.argv[1:])
