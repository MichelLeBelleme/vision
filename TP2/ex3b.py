# main.py
import sys
import cv2 as cv
import numpy as np
import math

def nothing(x):
	pass


def display():
    cv.createTrackbar('sigma','image 2',1,15,nothing)

def main(argv):
    filename = sys.argv[1]
    img = cv.imread(filename)
    cv.imshow("image 1", img)
    dst = img
    cv.imshow("image 2", dst)
    img2 = img
    cv.imshow("image 3", img2)
    img3 = img
    cv.imshow("image 4", img3)
    display()
    matrice = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
    matrice2 = np.array([[-1,-1,-1],[-1,9,-1],[-1,-1,-1]])


    while(1):
        cv.imshow("image 2", dst)
        cv.imshow("image 3", img2)
        cv.imshow("image 4", img3)
        sigmaX = cv.getTrackbarPos('sigma','image 2')

        n = math.floor(3*sigmaX)
        if n%2 == 0 :
            n+=1
        elif n<= 0:
            n=3

        dst = cv.GaussianBlur(img, (n,n), sigmaX)
        img2 = cv.filter2D(dst, -1, matrice)
        img3 = cv.filter2D(dst, -1, matrice2)
        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()
    








if __name__ == "__main__":
    main(sys.argv[1:])