# main.py
import sys
import cv2 as cv
import numpy as np
import math

def nothing(x):
	pass


def display():
    cv.createTrackbar('sigma','image 2',10,15,nothing)

def main(argv):
    filename = sys.argv[1]
    img = cv.imread(filename)
    cv.imshow("image 1", img)
    dst = img
    cv.imshow("image 2", dst)
    display()
    while(1):
        cv.imshow("image 2", dst)
        sigmaX = cv.getTrackbarPos('sigma','image 2')

        n = math.floor(3*sigmaX)
        if n%2 == 0 :
            n+=1
        elif n<= 0:
            n=3

        dst = cv.GaussianBlur(img, (n,n), sigmaX)
        
        print("sigmax",sigmaX)
        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()
    








if __name__ == "__main__":
    main(sys.argv[1:])