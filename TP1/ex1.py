# main.py
import sys
import cv2 as cv


def main(argv):

    filename = sys.argv[1]
    img = cv.imread(filename)
    print("Taille de l'image :\n\t-hauteur : ",img.shape[0] )
    print("\t-largeur : ", img.shape[1] )
    print("Nombre de canaux:", img.shape[2])
    print("Type des valeurs :",img.dtype )
    #Sépare l'image en ne créant des images qu'avec un seul channel
    r,g,b = cv.split(img)

    hsv = cv.cvtColor(img,cv.COLOR_RGB2HSV)
    t,s,v = cv.split(hsv)

    cv.imshow("Display Window1",img)
    k = cv.waitKey(0)
    cv.imshow("Display Window2",r)
    k = cv.waitKey(0)
    cv.imshow("Display Window3",g)
    k = cv.waitKey(0)
    cv.imshow("Display Window4",b)
    k = cv.waitKey(0)
    cv.imshow("Display Window5",hsv)
    k = cv.waitKey(0)
    cv.imshow("Display Window6",t)
    k = cv.waitKey(0)
    cv.imshow("Display Window7",s)
    k = cv.waitKey(0)
    cv.imshow("Display Window8",v)
    k = cv.waitKey(0)
    cv.imwrite("Panda_value.png", v)
    cv.imshow("Displapo Window",cv.imread("Panda_value.png"))
    k = cv.waitKey(0)



if __name__ == "__main__":
    main(sys.argv[1:])
