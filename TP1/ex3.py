# main.py
import sys
import cv2 as cv
import matplotlib.pyplot as plt


def main(argv):

    filename = sys.argv[1]
    img = cv.imread(filename)
    print("Taille de l'image :\n\t-hauteur : ",img.shape[0] )
    print("\t-largeur : ", img.shape[1] )
    print("Nombre de canaux:", img.shape[2])
    print("Type des valeurs :",img.dtype )
    r,g,b = cv.split(img)
    imgEq = cv.equalizeHist(r)


    hist =  cv.calcHist([img],[0],None,[256],[0,256])
    histEq =  cv.calcHist([imgEq],[0],None,[256],[0,256])
    cv.imshow("Display Window",img)
    k = cv.waitKey(0)
    cv.imshow("Display Window",imgEq)
    k = cv.waitKey(0)
    plt.plot(hist)
    plt.plot(histEq)
    plt.show()







if __name__ == "__main__":
    main(sys.argv[1:])
