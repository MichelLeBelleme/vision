# main.py
import sys
import cv2 as cv
import matplotlib.pyplot as plt


def main(argv):

    filename = sys.argv[1]
    img = cv.imread(filename)

    rhist =  cv.calcHist([img],[0],None,[256],[0,256])
    ghist =  cv.calcHist([img],[1],None,[256],[0,256])
    bhist =  cv.calcHist([img],[2],None,[256],[0,256])

    cv.imshow("Display Window",img)
    k = cv.waitKey(0)
    plt.plot(rhist)
    plt.plot(ghist)
    plt.plot(bhist)
    plt.show()







if __name__ == "__main__":
    main(sys.argv[1:])
