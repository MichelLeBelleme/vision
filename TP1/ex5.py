# main.py
import sys
import cv2 as cv
import matplotlib.pyplot as plt


def grab_frame(cap):
    ret,frame = cap.read()
    return cv.cvtColor(frame,cv.COLOR_BGR2RGB)

def main(argv):
    
    cap = cv.VideoCapture(0)
    if not cap.isOpened():
        print("Cannot open camera")
        exit()
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        # Our operations on the frame come here
        gray = cv.cvtColor(frame, cv.COLOR_RGB2GRAY)
        grayHist = cv.calcHist([frame],[0],None,[256],[0,256])
        
        #RGB histograms
        rhist =  cv.calcHist([frame],[0],None,[256],[0,256])
        ghist =  cv.calcHist([frame],[1],None,[256],[0,256])
        bhist =  cv.calcHist([frame],[2],None,[256],[0,256])
        #HSV frames
        hsv = cv.cvtColor(frame, cv.COLOR_RGB2HSV)
        h,s,v = cv.split(hsv)
        hsvEq = cv.equalizeHist(v)
        # Display the resulting frame
        cv.imshow('gray', gray)
        cv.imshow('rgb', frame)
        cv.imshow('hsv', hsv)
        cv.imshow('hsvEq', hsvEq)

        if cv.waitKey(1) == ord('q'):
            break
    cap.release()
    cv.destroyAllWindows()

    








if __name__ == "__main__":
    main(sys.argv)
