# main.py
import sys
import cv2 as cv
import matplotlib.pyplot as plt


def main(argv):

    filename = sys.argv[1]
    img = cv.imread(filename)
    r,g,b = cv.split(img)
    rEq = cv.equalizeHist(r)
    gEq = cv.equalizeHist(g)
    bEq = cv.equalizeHist(b)


    hsv = cv.cvtColor(img,cv.COLOR_RGB2HSV)
    t,s,v = cv.split(hsv)
    vEq = cv.equalizeHist(v)


    cv.imshow("Display Window",img)
    k = cv.waitKey(0)

    cv.imshow("Display Window1",r)
    k = cv.waitKey(0)
    cv.imshow("Display Window2",rEq)
    k = cv.waitKey(0)

    cv.imshow("Display Window3",g)
    k = cv.waitKey(0)
    cv.imshow("Display Window4",gEq)
    k = cv.waitKey(0)

    cv.imshow("Display Window5",b)
    k = cv.waitKey(0)
    cv.imshow("Display Window6",bEq)
    k = cv.waitKey(0)
    cv.imshow("Display Window6",vEq)
    k = cv.waitKey(0)








if __name__ == "__main__":
    main(sys.argv[1:])
