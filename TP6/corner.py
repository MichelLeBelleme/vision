# main.py
import sys
import cv2 as cv
import numpy as np
import math as m
import scipy.ndimage as sp


def norm(values):
    return np.array(values)


def main(argv):
    n = int(sys.argv[4])
    sigma = int(sys.argv[3])
    (h1, r1) = harris(sys.argv[1], sigma, n)
    (h2, r2) = harris(sys.argv[2], sigma, n)
    h = np.concatenate((h1, h2), axis=1)
    cv.imshow("show", h)
    while 1:
        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()


def harris(filename, sigma,  n):
    img = cv.imread(filename)

    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)

    harris = cv.cornerHarris(gray, 3, 3, 0.04)
    results = np.zeros(gray.shape)
    results[harris > 0.01*harris.max()] = sp.generic_filter(gray,
                                                            norm, size=(3, 3))[harris > 0.01*harris.max()]
    dst = img.copy()
    dst[harris > 0.01*harris.max()] = [0, 0, 255]

    return (dst, results)


if __name__ == "__main__":
    main(sys.argv[1:])
