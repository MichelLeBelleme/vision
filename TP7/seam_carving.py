# main.py
import sys
import cv2 as cv
import numpy as np
from skimage import transform, util
from skimage import filters, color

total = 0
def computeGx(img_in):
    matrice = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
    sobelx = cv.filter2D(img_in, cv.CV_64F, matrice)
    return sobelx

def computeGy(img_in):
    matrice = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
    sobely = cv.filter2D(img_in,cv.CV_64F, matrice)
    return sobely

def computeMagnitude(Gx,Gy):
    Gx = Gx.astype(np.uint32)
    Gy = Gy.astype(np.uint32)
    x2 = np.power(Gx,2)
    y2 = np.power(Gy,2)
    x2y2 = np.add(x2,y2)
    magnitude = np.sqrt(x2y2)
    magnitude = np.add((magnitude<=255)*magnitude,(magnitude>255)*255).astype(np.uint8)
    #print (magnitude)
    return magnitude.sum(axis=2)

def nothing(x):
    pass
    
def display(max):
    cv.createTrackbar('iteration','seam',0,max,nothing)

def computeMinTable(img):
    minimum = np.empty((img.shape[0],img.shape[1])).astype(np.uint32)
    jmin = np.empty((img.shape[0],img.shape[1])).astype(np.uint32)
    #print(img)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if i == 0:
                minimum[i][j] = img[i][j]
                jmin[i][j] = 0
            else : 
                jMin = previous(i,j,minimum)
                jmin[i][j] = jMin
                minimum[i][j] = minimum[i-1][jMin] +img[i][j]
    #print(minimum[minimum.shape[0]-1])
    return minimum,jmin

def previous(i,j,img):
    jMin = 0
    if j > 0 and j<img.shape[1]-1:
        jMin  = np.argmin(np.array([img[i-1][j-1],img[i-1][j],img[i-1][j+1]])) +j-1
    if j==0:
        jMin  = np.argmin(np.array([img[i-1][j],img[i-1][j+1]])) +j
    if j == img.shape[1]-1:
        jMin  = np.argmin(np.array([img[i-1][j-1],img[i-1][j]])) +j-1
    return jMin

def remove(img,min,chemin):
    jMin = 0
    newImg = np.empty((img.shape[0],img.shape[1]-1,3))
    jErase = np.argmin(min[img.shape[0]-1])
    print(jErase)
    for i in range(img.shape[0]):
        newImg[(img.shape[0]-1)-i] = np.delete(img[(img.shape[0]-1)-i],jErase,0)
        jErase = chemin[(img.shape[0]-1)-i][jErase]
    return newImg.astype(np.uint8)


def main(argv):
    filename = sys.argv[1]
    img = cv.imread(filename)
    gray = cv.cvtColor(img.copy(), cv.COLOR_RGB2GRAY )
    gx = img
    gy = img
    magnitude = img
    chemin = img
    print(img.shape)
    cv.imshow("img", img)
    max = -1

    if sys.argv[2] == 'x' :
        img  = np.swapaxes(img,0,1)
    if float(sys.argv[3])<1  :
        max = int((1-float(sys.argv[3])) * img.shape[1])
    elif float(sys.argv[3])<img.shape[1]:
        max = int(sys.argv[3])
    else :
        pass

    if max>0 :        
        for i in range(max):
            print(i)
            #print("Gray calculated")
            gx = computeGx(img)
            #print("Gx calculated")
            gy = computeGy(img)
            #print("Gy calculated")
            magnitude = computeMagnitude(gx,gy)
            #print("Energy calculated")
            min,chemin = computeMinTable(magnitude.copy())
            #print("Min table calculated")
            #print("Removing column")
            img = remove(img,min,chemin)
            #print("Removed")
            print(img.shape)
        if sys.argv[2] == 'x' :
            img = np.swapaxes(img,0,1)
        cv.imshow("seam", img)
        while(1):
            k = cv.waitKey(1) & 0xFF
            if k == 27:
                break
        cv.destroyAllWindows()
    else:
        print("Erreur paramètres non valides utilisez : python3 seam_carving (nom image) (axe x ou y) (nombre de d'itteration si a>=1  si a<1 ratio appliqué à l'image)")






if __name__ == "__main__":
    main(sys.argv[1:])