Pour ce rapport j'ai utilisé une image d'avion. Pour chaque transformation l'image obtenue sera suivi d'une remarque sur les points obtenus.

# 1. Harris

![Harris opencv sans transformation](./h11.PNG)

- Détecteur de coins Harris sans transformation

![Harris opencv Scaling](./h13.PNG)

- Résistance au Scaling : Bonne résistance au scaling très peu de points disparaissent ou change de place

![Harris opencv rotation](./h12.PNG)

- Résistance à la rotation : Bonne résistance à la rotation très peu de points disparaissent ou change de place

![Harris opencv bruit](./h14.PNG)

- Résistance au bruit : Mauvaise résistance au bruit car n'ayant pas de limite de point et se basant sur le gradiant le bruit provoque une détection de point dans un espace unicolore au départ

![Harris opencv transformation projective](./h15.PNG)

- Résistance à la transformation projective : Si la déformation n'est pas tros grande le détecteur de Harris reconnait des points se trouvant assez proche de la détection de départ

# 2. Harris implémentation

![Harris implémentation sans transformation](./h21.PNG)

- Implémentation du détecteur Harris, image sans transformation

![Harris implémentation Scaling](./h23.PNG)

- Résistance au Scaling : Bonne résistance au scaling très peu de points disparaissent ou change de place

![Harris implémentation rotation](./h22.PNG)

- Résistance à la rotation : Bonne résistance à la rotation très peu de points disparaissent ou change de place

![Harris implémentation bruit](./h24.PNG)

- Résistance au bruit : Mauvaise résistance au bruit car n'ayant pas de limite de point et se basant sur le gradiant le bruit provoque une détection de point dans un espace unicolore au départ

![Harris implémentation transformation projective](./h25.PNG)

- Résistance à la transformation projective : Si la déformation n'est pas tros grande le détecteur de Harris reconnait des points se trouvant assez proche de la détection de départ

L'implémentation et la fonction openCV du détecteur de coins de Harris sont très ressemblante si ce n'est identique sur la rotation le scalling et la transformation projective mais pas le bruit (un bruit différent étant appliqué). Sur la résistance au bruit on peut voir que les contours des ailes de l'avion sont quand même identifiés dans les deux cas

# 3. Shi-Tomasi

Les détections de points sont effectués avec un nombre de points maximum de 100
![Shi-Tomasi sans transformation](./st1.PNG)

- Détecteur de coins de Shi-Tomasi , image sans transformation

![Shi-Tomasi Scaling](./st2.PNG)

- Résistance au Scaling : Plus de points sont détecté par rapport à l'image sans transformation mais les points détectés dans l'image sans transformation peuvent se retrouver dans celle-ci

![Shi-Tomasi rotation](./st3.PNG)

- Résistance à la rotation : De même que pour le scaling il y plus de points détectés mais ceux-ci contiennent ceux de l'image sans transformation

![Shi-Tomasi bruit](./st4.PNG)

- Résistance au bruit : Du au nombre maximum de points détectable de 100 ce détecteur a une résistance qui dépend de ce paramètre. De ce fait un nombre plus ou moins important de points vont être détectés en plus la résistance est donc faible mais dépend du nombre maximum

![Shi-Tomasi transformation projective](./st5.PNG)

- Résistance à la transformation projective : De même que pour les autres transformations il y a des changements quant au nombre de points détectés. Comme auparavant les points détecté dans l'image sans transformation peuvent se retrouver dans celle-ci.

Les résistances dépendent du nombre de points maximum détectable par Shi-tomasi.

# 4. SIFT

Les détections de points sont effectués avec un nombre de points maximum de 100
![SIFT sans transformation](./si1.PNG)

- Détecteur SIFT , image sans transformation

![SIFT Scaling](./si3.PNG)

- Résistance au Scaling : Bonne résistance au scaling on peut tout de même apercevoir plus de points a certains endroits mais les points dans le ciel semblent être proche de l'image sans transformation

![SIFT rotation](./si2.PNG)

- Résistance à la rotation : Bonne résistance à la rotation de même que pour le scaling on peut voir plus de points a certains endroits notamment au niveau du changement de couleur de l'avion les points dans le ciel semblent être proche de l'image sans transformation comme pour le scaling

![SIFT bruit](./si4.PNG)

- Résistance au bruit : Résistance au bruit très mauvaise, des nuages de points apparaissent au niveau des points détecté sur l'image sans transformation

![SIFT transformation projective](./si5.PNG)

- Résistance à la transformation projective : Les points détectés on pour la plupart été décalé même si ils se trouvent dans des zones similaires la résistance à la transformation projective est donc mauvaises à cause du nombre de points décalés.

# 5. SURF

Les détections de points sont effectués avec un nombre de points maximum de 100
![SURF sans transformation](./su1.PNG)

- Détecteur SURF , image sans transformation

![SURF Scaling](./su2.PNG)

- Résistance au Scaling : Le nombre de points semblent un peu plus important mais les points d'intérêt se retrouvent aux même endroits la résistantce est donc de bonne qualité

![SURF rotation](./su3.PNG)

- Résistance à la rotation : Le nombre de points semblent un peu plus important mais les points d'intérêt se retrouvent aux même endroits la résistantce est donc de bonne qualité

![SURF bruit](./su4.PNG)

- Résistance au bruit : Mauvaise résistance au bruit à cause d'un nombre de points trop important

![SURF transformation projective](./su5.PNG)

- Résistance à la transformation projective : Les points détectés sont un peu décalés et de nouveaux apparaissent
