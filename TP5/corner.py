# main.py
import sys
import cv2 as cv
import numpy as np



def nothing(x):
    pass


def display():
    pass


def rotationTrackbar():
    cv.createTrackbar('angle', 'dst', 0, 360, nothing)
    cv.createTrackbar('scale', 'dst', 50, 100, nothing)


def noiseTrackbar():
    cv.createTrackbar('Bruit', 'dst', 0, 100, nothing)

def affineTrackbar(cols,rows):
    cv.createTrackbar('haut gauche x', 'dst', 0, cols, nothing)
    cv.createTrackbar('haut gauche y', 'dst', 0, rows, nothing)
    cv.createTrackbar('haut droit x', 'dst', 0, cols, nothing)
    cv.createTrackbar('haut droit y', 'dst', 0, rows, nothing)
    cv.createTrackbar('bas gauche x', 'dst', 0, cols, nothing)
    cv.createTrackbar('bas gauche y', 'dst', 0, rows, nothing)
    cv.createTrackbar('bas droit x', 'dst', 0, cols, nothing)
    cv.createTrackbar('bas droit y', 'dst', 0, rows, nothing)
    
    
    
   


def main(argv):
    method = sys.argv[2]
    if (method == "h1"):
        harris_opencv(sys.argv[1])
    elif (method == "h2"):
        harris_implementation(sys.argv[1])
    elif(method == "st"):
        shiTomasi(sys.argv[1])
    elif(method == "sift"):
        sift(sys.argv[1])
    elif(method == "surf"):
        surf(sys.argv[1])


def noiseUpdater(x):
    bruit = np.random.normal(0, x, size=cv.imread(sys.argv[1]).shape)


def harris_opencv(filename):
    img = cv.imread(filename)
    rot = img
    harris = rot
    dst = rot
    affine = rot
    bruit = np.random.normal(0, 0, size=cv.imread(sys.argv[1]).shape)
    size = 0
    newSize = 0
    noised = rot.astype(np.float32)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)
    cols = img.shape[1]
    rows = img.shape[0]
    cv.imshow("dst", dst)
    cv.imshow("show", dst)
    rotationTrackbar()
    noiseTrackbar()
    affineTrackbar(cols,rows)
    while 1:

        cv.imshow("dst", dst)
        cv.imshow("show", dst)

        newSize = cv.getTrackbarPos('Bruit', 'dst')
        angle = cv.getTrackbarPos('angle', 'dst')
        scale = cv.getTrackbarPos('scale', 'dst')
        hgx = cv.getTrackbarPos('haut gauche x', 'dst')
        hdx = cv.getTrackbarPos('haut droit x', 'dst')
        bgx = cv.getTrackbarPos('bas gauche x', 'dst')
        bdx = cv.getTrackbarPos('bas droit x', 'dst')
        hgy = cv.getTrackbarPos('haut gauche y', 'dst')
        hdy = cv.getTrackbarPos('haut droit y', 'dst')
        bgy = cv.getTrackbarPos('bas gauche y', 'dst')
        bdy = cv.getTrackbarPos('bas droit y', 'dst')

        if size != newSize:
            size = newSize
            bruit = np.random.normal(
                0, size, size=cv.imread(sys.argv[1]).shape)
        src_points = np.float32([[hgx, hgy], [bgx, rows-bgy], [cols-hdx, hdy], [cols-bdx, rows-bdy]])
        dst_points = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])
        

        M = cv.getPerspectiveTransform(src_points, dst_points)

        rotMat = cv.getRotationMatrix2D((0, 0), angle, scale/100+0.5)
        noised = (img + bruit).astype(np.uint8)
        affine = cv.warpPerspective(noised, M, (cols, rows))
        rot = cv.warpAffine(affine, rotMat, (cols, rows))
        #bruit = np.random.normal(0, 1, size=rot.shape)
        gray = cv.cvtColor(rot, cv.COLOR_RGB2GRAY)
        gray = np.float32(gray)
        harris = cv.cornerHarris(gray, 3, 3, 0.04)
        harris = cv.dilate(harris, None)
        dst = rot.copy()
        dst[harris > 0.01*harris.max()] = [0, 0, 255]

        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()


def computeGx(img_in):
    matrice = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
    sobelx = cv.filter2D(img_in, cv.CV_64F, matrice)
    return sobelx


def computeGy(img_in):
    matrice = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
    sobely = cv.filter2D(img_in, cv.CV_64F, matrice)
    return sobely


def cornerHarris(k, Ix2, Iy2, Ixy):
    det = np.float32(Ix2.shape)
    trace = np.float32(Ix2.shape)
    det = (Ix2*Iy2) - (Ixy*Ixy)
    trace = Ix2 + Iy2
    R = np.float32(Ix2.shape)
    R = det - k*trace*trace
    return R


def harris_implementation(filename):
    img = cv.imread(filename)
    rot = img
    harris = rot
    dst = rot
    sobelx = rot
    sobely = rot
    sig1 = rot
    sig2 = rot
    sig3 = rot
    matrice = np.ones((3, 3))
    size = 0
    newSize = 0
    bruit = np.random.normal(0, 0, size=cv.imread(sys.argv[1]).shape)
    noised = rot.astype(np.float32)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)
    cols = img.shape[1]
    rows = img.shape[0]
    cv.imshow("dst", dst)
    cv.imshow("show", dst)
    rotationTrackbar()
    noiseTrackbar()
    affineTrackbar(cols,rows)
    while 1:

        cv.imshow("dst", dst)
        cv.imshow("show", dst)
        angle = cv.getTrackbarPos('angle', 'dst')
        scale = cv.getTrackbarPos('scale', 'dst')
        newSize = cv.getTrackbarPos('Bruit', 'dst')
        hgx = cv.getTrackbarPos('haut gauche x', 'dst')
        hdx = cv.getTrackbarPos('haut droit x', 'dst')
        bgx = cv.getTrackbarPos('bas gauche x', 'dst')
        bdx = cv.getTrackbarPos('bas droit x', 'dst')
        hgy = cv.getTrackbarPos('haut gauche y', 'dst')
        hdy = cv.getTrackbarPos('haut droit y', 'dst')
        bgy = cv.getTrackbarPos('bas gauche y', 'dst')
        bdy = cv.getTrackbarPos('bas droit y', 'dst')

        if size != newSize:
            size = newSize
            bruit = np.random.normal(
                0, size, size=cv.imread(sys.argv[1]).shape)
        src_points = np.float32([[hgx, hgy], [bgx, rows-bgy], [cols-hdx, hdy], [cols-bdx, rows-bdy]])
        dst_points = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])

        M = cv.getPerspectiveTransform(src_points, dst_points)

        rotMat = cv.getRotationMatrix2D((0, 0), angle, scale/100+0.5)
        noised = (img + bruit).astype(np.uint8)
        affine = cv.warpPerspective(noised, M, (cols, rows))
        rot = cv.warpAffine(affine, rotMat, (cols, rows))
        gray = cv.cvtColor(rot, cv.COLOR_RGB2GRAY)
        sobelx = computeGx(gray)
        sobely = computeGy(gray)
        sig1 = cv.filter2D(sobelx*sobelx, cv.CV_64F, matrice)
        sig2 = cv.filter2D(sobely*sobely, cv.CV_64F, matrice)
        sig3 = cv.filter2D(sobelx*sobely, cv.CV_64F, matrice)
        harris = cornerHarris(0.04, sig1, sig2, sig3)
        harris = cv.dilate(harris, None)
        dst = rot
        dst[harris > 0.01*harris.max()] = [0, 0, 255]

        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()


def shiTomasi(filename):
    img = cv.imread(filename)
    rot = img
    affine = rot
    bruit = np.random.normal(0, 0, size=cv.imread(sys.argv[1]).shape)
    size = 0
    newSize = 0
    noised = rot.astype(np.float32)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)
    cols = img.shape[1]
    rows = img.shape[0]
    cv.imshow("dst", rot)
    cv.imshow("show", rot)
    rotationTrackbar()
    noiseTrackbar()
    affineTrackbar(cols,rows)
    while 1:

        cv.imshow("dst", rot)
        cv.imshow("show", rot)
        newSize = cv.getTrackbarPos('Bruit', 'dst')
        angle = cv.getTrackbarPos('angle', 'dst')
        scale = cv.getTrackbarPos('scale', 'dst')
        hgx = cv.getTrackbarPos('haut gauche x', 'dst')
        hdx = cv.getTrackbarPos('haut droit x', 'dst')
        bgx = cv.getTrackbarPos('bas gauche x', 'dst')
        bdx = cv.getTrackbarPos('bas droit x', 'dst')
        hgy = cv.getTrackbarPos('haut gauche y', 'dst')
        hdy = cv.getTrackbarPos('haut droit y', 'dst')
        bgy = cv.getTrackbarPos('bas gauche y', 'dst')
        bdy = cv.getTrackbarPos('bas droit y', 'dst')

        if size != newSize:
            size = newSize
            bruit = np.random.normal(
                0, size, size=cv.imread(sys.argv[1]).shape)
        src_points = np.float32([[hgx, hgy], [bgx, rows-bgy], [cols-hdx, hdy], [cols-bdx, rows-bdy]])
        dst_points = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])

        M = cv.getPerspectiveTransform(src_points, dst_points)

        rotMat = cv.getRotationMatrix2D((0, 0), angle, scale/100+0.5)
        noised = (img + bruit).astype(np.uint8)
        affine = cv.warpPerspective(noised, M, (cols, rows))
        rot = cv.warpAffine(affine, rotMat, (cols, rows))

        gray = cv.cvtColor(rot, cv.COLOR_RGB2GRAY)
        gray = np.float32(gray)
        shiTomasi = cv.goodFeaturesToTrack(gray, 100, 0.01, 10)
        shiTomasi = np.int0(shiTomasi)

        for i in shiTomasi:
            x, y = i.ravel()
            cv.circle(rot, (x, y), 3, 255, -1)

        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()

def sift(filename):
    img = cv.imread(filename)
    rot = img
    affine = rot
    bruit = np.random.normal(0, 0, size=cv.imread(sys.argv[1]).shape)
    size = 0
    newSize = 0
    noised = rot.astype(np.float32)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)
    cols = img.shape[1]
    rows = img.shape[0]
    cv.imshow("dst", rot)
    cv.imshow("show", rot)
    rotationTrackbar()
    noiseTrackbar()
    affineTrackbar(cols,rows)
    while 1:

        cv.imshow("dst", rot)
        cv.imshow("show", rot)

        newSize = cv.getTrackbarPos('Bruit', 'dst')
        angle = cv.getTrackbarPos('angle', 'dst')
        scale = cv.getTrackbarPos('scale', 'dst')
        hgx = cv.getTrackbarPos('haut gauche x', 'dst')
        hdx = cv.getTrackbarPos('haut droit x', 'dst')
        bgx = cv.getTrackbarPos('bas gauche x', 'dst')
        bdx = cv.getTrackbarPos('bas droit x', 'dst')
        hgy = cv.getTrackbarPos('haut gauche y', 'dst')
        hdy = cv.getTrackbarPos('haut droit y', 'dst')
        bgy = cv.getTrackbarPos('bas gauche y', 'dst')
        bdy = cv.getTrackbarPos('bas droit y', 'dst')

        if size != newSize:
            size = newSize
            bruit = np.random.normal(
                0, size, size=cv.imread(sys.argv[1]).shape)
        src_points = np.float32([[hgx, hgy], [bgx, rows-bgy], [cols-hdx, hdy], [cols-bdx, rows-bdy]])
        #src_points = np.float32([[430, 200], [586, rows-158], [cols-266, 182], [cols-395, rows-11]])
        dst_points = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])

        M = cv.getPerspectiveTransform(src_points, dst_points)

        rotMat = cv.getRotationMatrix2D((0, 0), angle, 1.24)
        noised = (img + bruit).astype(np.uint8)
        affine = cv.warpPerspective(noised, M, (cols, rows))
        rot = cv.warpAffine(affine, rotMat, (cols, rows))

        gray = cv.cvtColor(rot, cv.COLOR_RGB2GRAY)
        gray = np.float32(gray)
        gray = np.uint8(gray)
        sift = cv.xfeatures2d.SIFT_create()
        kp = sift.detect(gray,None)
        #sift=cv.drawKeypoints(rot,kp,rot,color=(0,255,100))
        
        for i in cv.KeyPoint_convert(kp):
            x, y = i.ravel()
            cv.circle(rot, (x, y), 3, 255, -1)

        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()

def surf(filename):
    img = cv.imread(filename)
    rot = img
    affine = rot
    bruit = np.random.normal(0, 0, size=cv.imread(sys.argv[1]).shape)
    size = 0
    newSize = 0
    noised = rot.astype(np.float32)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    gray = np.float32(gray)
    cols = img.shape[1]
    rows = img.shape[0]
    cv.imshow("dst", rot)
    cv.imshow("show", rot)
    rotationTrackbar()
    noiseTrackbar()
    affineTrackbar(cols,rows)
    while 1:

        cv.imshow("dst", rot)
        cv.imshow("show", rot)

        newSize = cv.getTrackbarPos('Bruit', 'dst')
        angle = cv.getTrackbarPos('angle', 'dst')
        scale = cv.getTrackbarPos('scale', 'dst')
        hgx = cv.getTrackbarPos('haut gauche x', 'dst')
        hdx = cv.getTrackbarPos('haut droit x', 'dst')
        bgx = cv.getTrackbarPos('bas gauche x', 'dst')
        bdx = cv.getTrackbarPos('bas droit x', 'dst')
        hgy = cv.getTrackbarPos('haut gauche y', 'dst')
        hdy = cv.getTrackbarPos('haut droit y', 'dst')
        bgy = cv.getTrackbarPos('bas gauche y', 'dst')
        bdy = cv.getTrackbarPos('bas droit y', 'dst')

        if size != newSize:
            size = newSize
            bruit = np.random.normal(
                0, size, size=cv.imread(sys.argv[1]).shape)
        src_points = np.float32([[430, 200], [586, rows-158], [cols-266, 182], [cols-395, rows-11]])
        dst_points = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])

        M = cv.getPerspectiveTransform(src_points, dst_points)

        rotMat = cv.getRotationMatrix2D((0, 0), angle, 1.24)
        noised = (img + bruit).astype(np.uint8)
        affine = cv.warpPerspective(noised, M, (cols, rows))
        rot = cv.warpAffine(affine, rotMat, (cols, rows))

        gray = cv.cvtColor(rot, cv.COLOR_RGB2GRAY)
        gray = np.float32(gray)
        gray = np.uint8(gray)
        surf = cv.xfeatures2d.SURF_create(400)
        kp = surf.detect(gray,None)
        
        
        for i in cv.KeyPoint_convert(kp):
            x, y = i.ravel()
            cv.circle(rot, (x, y), 3, 255, -1)

        k = cv.waitKey(1) & 0xFF
        if k == 27:
            break
    cv.destroyAllWindows()



if __name__ == "__main__":
    main(sys.argv[1:])
